<?php
session_start();
require_once 'config.php';

// Check if the user is logged in
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
    exit();
}

// Delete blog post
if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    $sql = "DELETE FROM blogs WHERE id = '$id'";
    $result = $conn->query($sql);
}

// Fetch all blog posts
$sql = "SELECT * FROM blogs";
$result = $conn->query($sql);
$blogs = [];
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $blogs[] = $row;
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Mini Blog Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
    crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-info bg-info">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">Mini Blog </span>
            
            
        </div>  
        <div class="position-absolute top-0 end-0" button type="button" class="badge bg-primary text-wrap"style="width: 6rem;"><a href="index.php?logout=true">Logout</a></button></div>
    </nav>
    <div class="row">
        <div class="card">
            <div class="card-body">
                    <div class="container-secondary">
                        <br><br>
                        <?php foreach ($blogs as $blog): ?>
                            <div class="container">
                                <h3><?php echo $blog['title']; ?></h3>
                                <p><?php echo $blog['content']; ?></p>
                                <a href="dashboard.php?delete=<?php echo $blog['id']; ?>" onclick="return confirm('Are you sure you want to delete this blog?')"button type="button" class="btn btn-primary btn-sm"></button>DELETE</a>
                                <a href="edit.php?id=<?php echo $blog['id']; ?>"button type="button" class="btn btn-primary btn-sm"></button> EDIT</a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <br>
            </div>
        </div>   
    </div> <br> 
    <div class="card w-75">
        <div class="card-body">
            <div button type="button" class="btn btn-light"><a href="create.php">CREATE NEW BLOG</a></button></div>           
        </div>
    </div>
</body>
</html>
