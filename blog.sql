CREATE TABLE IF NOT EXISTS  `blogs` (
    `id` INT(11) PRIMARY KEY AUTO_INCREMENT,
    `title` VARCHAR(255),
    `content` TEXT,
    PRIMARY KEY (`id`)
);