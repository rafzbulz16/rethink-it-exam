<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <?php
        require('db.php');
        session_start();
        // If form submitted, insert values into the database.
        if (isset($_POST['username'])){
            
            $username = stripslashes($_REQUEST['username']); // removes backslashes
            $username = mysqli_real_escape_string($con,$username); //escapes special characters in a string
            $password = stripslashes($_REQUEST['password']);
            $password = mysqli_real_escape_string($con,$password);
            
        //Checking is user existing in the database or not
            $query = "SELECT * FROM `users` WHERE username='$username' and password='".md5($password)."'";
            $result = mysqli_query($con,$query) or die(mysql_error());
            $rows = mysqli_num_rows($result);
            if($rows==1)
            {
                $_SESSION['username'] = $username;
                header("Location: config.php"); // Redirect user to main phase
            }else
                {
                    echo "<div class='container'>
                    <h3> Username/password is incorrect.</h3>
                    <br/>Click here to <a href='login.php'>Login</a></div>";
                }
        }
    ?>

<div class="form container" style="margin-top: 10%;">
	<div class="row">
		<div class="jumbotron col-md-6 col-md-offset-3">
			<h1><center>LOGIN</center></h1>
			<form action="" method="post" name="login">
				<div class="form-group">
					<label class="label">Username</label>
					<input autocomplete="off" name="username" class="input form-control" type="text"required>
				</div>
				<div class="form-group">
					<label class="label">Password</label>
					<input autocomplete="off" name="password" class="input form-control" type="password" required>
				</div>
				<button class="btn btn-primary btn-block" name="submit" type="submit">Login</button>
			</form>
			<p>Not registered yet? <a href='registration.php'>Register Here</a></p>
		</div>
	</div>
</div>
</html>