<?php
session_start();
require_once 'config.php';

// Check if the user is logged in
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
    exit();
}

// Handle form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $title = $_POST['title'];
    $content = $_POST['content'];

    // Insert the blog post into the database
    $sql = "INSERT INTO blogs (title, content) VALUES ('$title', '$content')";
    $result = $conn->query($sql);

    if ($result) {
        // Blog post created successfully
        header('Location: dashboard.php');
        exit();
    } else {
        // Blog post creation failed
        header('Location: create.php?error=Blog post creation failed. Please try again.');
        exit();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Create Blog Post</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
    crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-info bg-info">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">Mini Blog</span>
        </div>  
        <div class="position-absolute top-0 end-0" button type="button" class="badge bg-primary text-wrap"style="width: 6rem;"><a href="index.php">Logout</a></button></div>
    </nav>
    <div class="container">
        <h2>Create Blog Post</h2>
        <form action="create.php" method="POST">
            <input type="text" name="title" placeholder="Title" required><br><br>
            <textarea name="content" placeholder="Content" required></textarea><br><br>
            <input type="submit" value="POST"button type="button" class="btn btn-primary"></button>
        </form>
        <br>
        <div class="card w-75">
            <div class="card-body">
                <div button type="button" class="btn btn-light"><a href="dashboard.php">Back to Dashboard</a></button>
                </div>           
            </div>
        </div>
    </div>
</body>
</html>
